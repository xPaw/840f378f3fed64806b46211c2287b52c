#!/bin/bash

openssl ecparam -name prime256v1 -genkey -out localhost.key

openssl req -new -x509 -days 7300 -out localhost.crt -key localhost.key \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost, DNS:*.localhost, IP:127.0.0.1, IP:::1\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

openssl dhparam -out dhparam.pem 4096
